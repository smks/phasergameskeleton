import {IState} from './../interfaces/istate';

export class Preloader extends Phaser.State implements IState {

    loadingText: Phaser.Text;

    preload() {

        this.loadingText = this.game.add.text(this.game.world.centerX, this.game.world.centerY, "Loading", {
            font: "65px Open Sans",
            fill: "#ffffff",
            align: "center"
        });

        this.loadingText.anchor.set(0.5, 0.5);

        /** Load Logo */
        //this.game.load.image('logo', assets + 'logo.png');

        /** Load spritesheet */
        //this.game.load.spritesheet("game", assets + 'diamonds32x5.png', 32, 32);

        /** Load JSON configuration */
        //this.game.load.json('level1', assets+'levels/level1.json');

        /** Load Audio */
        //this.game.load.audio('swapSound', assets+'sounds/sound.wav');
    }

    update() {

    }

    create() {
        this.game.state.start('MainMenu', true);
    }

    destroy() {

    }
}