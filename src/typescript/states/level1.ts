export class Level1 extends Phaser.State {
    private levelText;
    create() {
        this.levelText = this.game.add.text(
            this.game.world.centerX,
            this.game.world.centerY,
            "Level 1",
            {
                font: "65px Open Sans",
                fill: "#ffffff",
                align: "center"
            }
        );

        this.levelText.anchor.set(0.5, 0.5);
    }
    destroy() {
    }
}
