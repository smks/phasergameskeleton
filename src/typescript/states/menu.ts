///<reference path="../translation/english.ts"/>
import {IState} from "../interfaces/istate";
import {ITranslate} from "../interfaces/itranslate";
import {DutchTranslation} from "../translation/dutch";

export class Menu extends Phaser.State implements IState {

    private translation: ITranslate;
    private menuText;

    constructor() {
        super();
    }

    create() {

        this.translation = new DutchTranslation();

        this.menuText = this.game.add.text(
            this.game.world.centerX,
            this.game.world.centerY,
            this.translation.translate('a'),
            {
                font: "65px Open Sans",
                fill: "#ffffff",
                align: "center"
            }
        );

        this.menuText.anchor.set(0.5, 0.5);
    }
    destroy() {
    }
}