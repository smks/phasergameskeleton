/// <reference path='../../node_modules/phaser/typescript/phaser' />

import Game = Phaser.Game;

import {Boot} from "./states/boot";
import {Preloader} from "./states/preload";
import {Level1} from "./states/level1";
import {Complete} from "./states/complete";
import {Menu} from "./states/menu";

class MyGame {
    game:Game;

    constructor() {

        this.game = new Game(
            960,
            540,
            Phaser.AUTO,
            'game', {
                create: this.create
            }
        );
    }

    create() {

        this.game.state.add('Boot', Boot, false);
        this.game.state.add('Preloader', Preloader, false);
        this.game.state.add('MainMenu', Menu, false);
        this.game.state.add('Level1', Level1, false);
        this.game.state.add('Complete', Complete, false);

        this.game.state.start('Boot');
    }

    public destroy() {

    }
}


window.onload = () => {
    var game = new MyGame();
}