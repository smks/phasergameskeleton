import {ITranslate} from './../interfaces/itranslate';

export class DutchTranslation implements ITranslate {

    /**
     *
     * @type {{a: string, b: string, c: string}}
     */
    private translationMap = {
        'a': 'Dit is een',
        'b': 'Dit is B',
        'c': 'Dit C'
    }

    /**
     *
     * @param key
     * @returns {string}
     */
    translate(key:string):string {
        if (this.translationMap[key] !== null) {
            return this.translationMap[key];
        }

        return key;
    }
}