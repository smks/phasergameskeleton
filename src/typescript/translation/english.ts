import {ITranslate} from './../interfaces/itranslate';

export class EnglishTranslation implements ITranslate {

    private translationMap = {
        'a': 'This is A',
        'b': 'This is B',
        'c': 'This is C'
    }

    public translate(key:string):string {
        if (this.translationMap[key] !== null) {
            return this.translationMap[key];
        }

        return key;
    }
}