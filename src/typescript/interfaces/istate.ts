export interface IState {
    create();
    update();
    destroy();
}