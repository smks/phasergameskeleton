"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Level1 = (function (_super) {
    __extends(Level1, _super);
    function Level1() {
        _super.apply(this, arguments);
    }
    Level1.prototype.create = function () {
        this.levelText = this.game.add.text(this.game.world.centerX, this.game.world.centerY, "Level 1", {
            font: "65px Open Sans",
            fill: "#ffffff",
            align: "center"
        });
        this.levelText.anchor.set(0.5, 0.5);
    };
    Level1.prototype.destroy = function () {
    };
    return Level1;
}(Phaser.State));
exports.Level1 = Level1;
