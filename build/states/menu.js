"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var dutch_1 = require("../translation/dutch");
var Menu = (function (_super) {
    __extends(Menu, _super);
    function Menu() {
        _super.call(this);
    }
    Menu.prototype.create = function () {
        this.translation = new dutch_1.DutchTranslation();
        this.menuText = this.game.add.text(this.game.world.centerX, this.game.world.centerY, this.translation.translate('a'), {
            font: "65px Open Sans",
            fill: "#ffffff",
            align: "center"
        });
        this.menuText.anchor.set(0.5, 0.5);
    };
    Menu.prototype.destroy = function () {
    };
    return Menu;
}(Phaser.State));
exports.Menu = Menu;
