"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Preloader = (function (_super) {
    __extends(Preloader, _super);
    function Preloader() {
        _super.apply(this, arguments);
    }
    Preloader.prototype.preload = function () {
        this.loadingText = this.game.add.text(this.game.world.centerX, this.game.world.centerY, "Loading", {
            font: "65px Open Sans",
            fill: "#ffffff",
            align: "center"
        });
        this.loadingText.anchor.set(0.5, 0.5);
    };
    Preloader.prototype.update = function () {
    };
    Preloader.prototype.create = function () {
        this.game.state.start('MainMenu', true);
    };
    Preloader.prototype.destroy = function () {
    };
    return Preloader;
}(Phaser.State));
exports.Preloader = Preloader;
