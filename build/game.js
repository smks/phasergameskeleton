"use strict";
var Game = Phaser.Game;
var boot_1 = require("./states/boot");
var preload_1 = require("./states/preload");
var level1_1 = require("./states/level1");
var complete_1 = require("./states/complete");
var menu_1 = require("./states/menu");
var MyGame = (function () {
    function MyGame() {
        this.game = new Game(960, 540, Phaser.AUTO, 'game', {
            create: this.create
        });
    }
    MyGame.prototype.create = function () {
        this.game.state.add('Boot', boot_1.Boot, false);
        this.game.state.add('Preloader', preload_1.Preloader, false);
        this.game.state.add('MainMenu', menu_1.Menu, false);
        this.game.state.add('Level1', level1_1.Level1, false);
        this.game.state.add('Complete', complete_1.Complete, false);
        this.game.state.start('Boot');
    };
    MyGame.prototype.destroy = function () {
    };
    return MyGame;
}());
window.onload = function () {
    var game = new MyGame();
};
