"use strict";
var DutchTranslation = (function () {
    function DutchTranslation() {
        this.translationMap = {
            'a': 'Dit is een',
            'b': 'Dit is B',
            'c': 'Dit C'
        };
    }
    DutchTranslation.prototype.translate = function (key) {
        if (this.translationMap[key] !== null) {
            return this.translationMap[key];
        }
        return key;
    };
    return DutchTranslation;
}());
exports.DutchTranslation = DutchTranslation;
