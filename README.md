Phaser Skeleton with TypeScript
=====

## Requirements

* [Node.js](http://www.nodejs.org)
* [http-server](https://www.npmjs.com/package/http-server)
* [TypeScript](http://www.typescriptlang.org/)
* [Gulp](http://gulpjs.com/)

## Subscribe to Firebase

<script src="https://www.gstatic.com/firebasejs/3.6.1/firebase.js"></script>

`
   var config = {
        apiKey: "AIzaSyCmV47bt6pB6_ixV5okB3WKQrTgdpJ31hA",
        authDomain: "bunnybombs-cdfd2.firebaseapp.com",
        databaseURL: "https://bunnybombs-cdfd2.firebaseio.com",
        storageBucket: "bunnybombs-cdfd2.appspot.com",
        messagingSenderId: "978640014563"
    };
    firebase.initializeApp(config);
`